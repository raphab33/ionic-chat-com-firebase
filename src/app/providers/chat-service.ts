import { Injectable } from '@angular/core';

export class ChatMessage {
  messageId: string;
  userId: string;
  userName: string;
  userAvatar: string;
  time: number | string;
  message: string;
  email: string;
  status: string;
  medico: boolean;
}

@Injectable()
export class ChatService {
  
}
