import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FormsModule } from '@angular/forms';
import { RelativeTime } from './pipes/relative-times';
import { EmojiProvider } from './providers/emoji';
import { ChatService } from './providers/chat-service';
import { HttpClientModule } from "@angular/common/http";

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';

const firebase = {
  apiKey: "AIzaSyDasuLkSFk5M_bmbzyGlZqBCmF0K8cdRC8",
  authDomain: "projeto3-57dd0.firebaseapp.com",
  databaseURL: "https://projeto3-57dd0.firebaseio.com",
  projectId: "projeto3-57dd0",
  storageBucket: "projeto3-57dd0.appspot.com",
  messagingSenderId: "322474070233",
  appId: "1:322474070233:web:c1b84605f5c537a7"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RelativeTime
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebase),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    EmojiProvider,
    ChatService,
    AngularFirestore,
    
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
