import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { ChatMessage } from '../../app/providers/chat-service';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [AngularFireAuth], 
})


export class HomePage implements OnInit  {
  
  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;
  
  private salaCollection: AngularFirestoreCollection<ChatMessage>;
  sala: Observable<ChatMessage[]>;
  msgList: ChatMessage[] = [];
  user: any;
  userEmail: string;
  editorMsg = '';
  nMsg: number;
  nomeUsuario: string;

  constructor(
      private afs: AngularFirestore,
      private afAuth: AngularFireAuth,
      ) {
  }


  ngOnInit() {
    this.buscarMensagens();
    this.verificarSeAutenticado(); 
    this.scrollToBottomInicial();
  }

  // verifica se o usuaio está autenticado
  async verificarSeAutenticado() {
    this.afAuth.authState.subscribe(usr => {
      if(usr) {
        this.user = usr;
        this.userEmail = usr.email;
        this.buscarMensagens(); 
        this.scrollToBottomInicial();
      }
    });
  }

  // buca as mensagens no firebase::::Na integração adicionar o n° do atendimento a sala
  async buscarMensagens() {
      this.salaCollection = this.afs.collection<ChatMessage>('sala', ref => ref.orderBy('messageId'));
      this.sala = this.salaCollection.valueChanges();
      this.verifiarMudanca();
  }

  // verifica no firebase novas mensagens
  verifiarMudanca() {
      this.salaCollection.valueChanges().subscribe(() => {
        // this.nMsg = data.length;
        this.scrollMsg();
      })
  }

  autenticarFacebook() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then(res => {
      this.user = res.user;
      this.userEmail = res.user.email;
    });
  }

  autenticarEmail1() {
    this.afAuth.auth.signInWithEmailAndPassword('raphab33@gmail.com', '123456')
    .then((res) => {
      console.log(res.uid);
      this.user =  res;
      this.userEmail = res.email;
      // this.nomeUsuario = 'Rafael';
      console.log(this.user);
    })
  }


  autenticarEmail2() {
    this.afAuth.auth.signInWithEmailAndPassword('teste@hotmail.com', '123456')
    .then((res) => {
      console.log(res.uid);
      this.user =  res;
      this.userEmail = res.email;
      // this.nomeUsuario = 'Fulano';
    })
  }

  // sair
  logout() {
    this.afAuth.auth.signOut(); // retira a autenticação do firebase
    this.user = ''; // limpa o usuario logado
    this.userEmail = ''; //limpa o email logado
  }
 
  // dar foco quando clicar no campo
  onFocus() {
    this.content.resize(); // aumenta o campo
    this.scrollToBottom(); // rola o scroll
  }

  // Função para dar foco no campo
  focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }
  
  // Passar dados dos usuarios
  sendMsg() {
    if (!this.editorMsg.trim()) return;
    if(this.user) {
      let newMsg: ChatMessage = {
        messageId: Date.now().toString(),
        userId: this.user.uid,
        userName: this.user.displayName,
        // userName: this.nomeUsuario,
        userAvatar: this.user.photoURL,
        email: this.userEmail,
        time:  Date.now(),
        message: this.editorMsg,
        status: 'success',
        medico: true
      };
  
      this.adicionarMsg(newMsg);
    }
  
  }

  // salvar mensagem no firebase
  async adicionarMsg(msg) {
      this.salaCollection.add(msg); //adicionar
      this.editorMsg = ''; // limpar campo
      this.scrollToBottom(); //rolar
  }

  // Rolar o scroll quando o usuario digitar a mensagem
  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 350)
  }

  // Rolar o scroll quando iniciar o chat 
  scrollToBottomInicial() {
    setTimeout(() => {
        this.content.scrollToBottom();
    }, 1200)
  }

  // Rolar as mensagens quando receber de outro usuario
  async scrollMsg() {
    setTimeout(() => {
        this.content.scrollToBottom();
    }, 400)
  }

  
  // Função para subir e descer o campo
  setTextareaScroll() {
    const textarea = this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
    this.scrollToBottom();
  }
}
